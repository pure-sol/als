<?php
namespace ALS\Modules\User\Providers;

class ModuleServiceProvider extends \ALS\Providers\ModuleServiceProvider
{
    static $routesPaths = [
        __DIR__ . '/../routes/routes.php'
    ];

    public function getModuleName(): string
    {
        return 'User';
    }
}